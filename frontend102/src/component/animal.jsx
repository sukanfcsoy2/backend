import React, { useState } from 'react'
import './use_state_object_example.css'
import axios from 'axios';

export default function Animal() {
    const intialState = { name: "", weight: 0, height: 0};
    const [animal, setAnimal] = useState(intialState);
    const [animalList, setAnimalList] = useState([]);
  
    function SaveAnimal() {
        axios.post('http://localhost:888/animal/add',animal)
        .then(response =>{
            console.log(response.toString());
            getAnimal();
        })
        .catch(error => console.log(error));
    }
    function getAnimal() {
        axios.get('http://localhost:888/animal')
        .then(response =>{
            console.log(response);
            if (response.data) {
              setAnimalList(response.data);
            }
        })
        .catch(error => console.log(error));
    }
    function deleteAnimal(id) {
        axios.delete('http://localhost:888/animal/delete/' + id )
        .then(response =>{
            console.log(response);
            getAnimal();
        })
        .catch(error => console.log(error));
    }


  return (
  
    <div>
      <div className='container2'>
        Animal From
      </div>
      <div className='container2'>
            Name : <input onChange={(e) => setAnimal({...animal, name: e.target.value})} />
        </div>
      <div className='container2'>
            Weight : <input type='number' onChange={(e) => setAnimal({...animal, weight: e.target.value})} />
        </div>
      <div className='container2'>
            Height : <input type='number' onChange={(e) => setAnimal({...animal, height: e.target.value})} />
        </div>
      <div className='container2'>
            { JSON.stringify(animal) }
        </div>
     <div className='container2'>
            <button onClick={SaveAnimal} >SaveAnimal</button>
        </div>
     <div className='container2'>
            <button onClick={getAnimal} >GetAnimal</button>
        </div>
      
     <div>
            <table>
                <thead>
                    <th key={1}>Name</th>
                    <th key={2}>Weight</th>
                    <th key={3}>Height</th>
                    <th key={4}>Delete</th>
                </thead>
                <tbody>
                    { animalList.map(item => <tr key={item._id}>
                       <td>{item.name ?? ''}</td>
                       <td>{item.weight ?? ''}</td>
                       <td>{item.height ?? ''}</td>
                       <td>
                        <button onClick={() => deleteAnimal(item._id)}>Delete</button>
                       </td>
                    </tr>) }
                </tbody>
            </table>
        </div>
    </div>
  )
}
