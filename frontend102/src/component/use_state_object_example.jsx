import React, { useState } from 'react'
import './use_state_object_example.css'

export default function UsestateObjectExample() {
    const intialState = { name: "", weight: 0, height: 0};
    const [animal, setAnimal] = useState(intialState);
  
    function SaveAnimal() {
        alert('SaveComplet')
    }

  return (
    <div>
      <div className='container2'>
        Animal From
      </div>
      <div className='container2'>
            Name : <input onChange={(e) => setAnimal({...animal, name: e.target.value})} />
        </div>
      <div className='container2'>
            Weight : <input type='number' onChange={(e) => setAnimal({...animal, weight: e.target.value})} />
        </div>
      <div className='container2'>
            Height : <input type='number' onChange={(e) => setAnimal({...animal, height: e.target.value})} />
        </div>
     <div className='container2'>
            <button onClick={SaveAnimal} >SaveAnimal</button>
      </div>
     

        <div className='container2'>
            { JSON.stringify(animal) }
        </div>
    </div>
  )
}
