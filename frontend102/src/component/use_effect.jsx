import React, { useEffect, useState } from 'react'
export default function UseEffectExample() {
    const [counter, setCounter] = useState(0);
    useEffect(() => {
        document.title = 'Current count ' + counter;

    }, [counter]);

  return (
    <div>
        UseEffectExample { counter }
        <div>
            <button onClick={() => setCounter(previousValue => previousValue + 1)}>
                Add
            </button>
        </div>
    </div>
  )
}
