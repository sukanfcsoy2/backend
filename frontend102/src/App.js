import logo from './logo.svg';
import './App.css';
//import UsestateObjectExample from './component/use_state_object_example';
// import FunctionComponent  from './component/function_Component';
// import Class_component from './component/class_component';
// import UseStateExample from './component/use_state_example';
//import Useeffect from './component/use_effect';
import Animal from './component/animal';

function App() {
  return (
    <div className="App">
      <header>

       <div className='container'>
        {/* <FunctionComponent> </FunctionComponent>
        <Class_component> </Class_component>
        <UseStateExample> </UseStateExample>
        <UsestateObjectExample></UsestateObjectExample>
        <Useeffect></Useeffect> */}
        <Animal> </Animal>
       </div>

      </header>
    </div>
  );
}

export default App;
